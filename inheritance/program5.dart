class parent{
	int x=10;
	parent(){
		print("in parent");
		print(hashCode);
	}
	void printData(){
		print(x);
	}
}
class child extends parent{
	int x=20;
	child(){
		print("in child");
		print(hashCode);
	}
	void dispData(){
		print(x);
	}
}
void main(){
	child obj =new child();
	obj.dispData();
	obj.printData();
}
