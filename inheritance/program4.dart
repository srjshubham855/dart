class parent{
	int x=10;
	String str="name";

	void parentInfo(){
		print(x);
		print(str);
	}
}
class child extends parent{
	int x=20;
	String str="surname";

	void childInfo(){
		print(x);
		print(str);

	}
}
void main(){
	child obj=new child();
	parent obj1=new parent();
	obj.childInfo();
	obj.parentInfo();
	print(obj.x);
	obj1.parentInfo();
}
