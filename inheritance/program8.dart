class parent{
	int? x;
	String? str;

	parent(this.x,this.str);

	void printData(){
		print(x);
		print(str);
	}
	/*call(){
		print("in call method");
}*/
}

 
class child extends parent{
	int? y;
	String? name;

	
	child(this.y,this.name,int x,String str):super(x,str);
	
 		
  	void dispData(){
		print(y);
		print(name);
	}
}
void main(){
	child obj=new child(10,"shubham",21,"saroj");
	obj.printData();
	obj.dispData();
}
