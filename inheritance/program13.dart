class parent{
	parent(){
		print("parent const");
	}
}
class child1 extends parent{
	child1(){
		print ("child1 const");
	}
}
class child2 extends parent{
	child2(){
		print("child2 const");
	}
}
void main(){
	parent obj1=new parent();
	child1 obj2=new child1();
	child2 obj3=new child2();
}
