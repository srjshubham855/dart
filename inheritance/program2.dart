class parent{
	int x=10;
	String str="Shubham";
		
	void Disp(){
		print(x);
		print(str);
	}
}
class child extends parent{
	int y=20;
	String str1="Saroj";

	void printData(){
		print(y);
		print(str1);
	}
}
void main(){

	parent obj1=new parent();
	obj1.Disp();
	print(obj1.x);
	print(obj1.str);

	print("------------------------------------------");

	child obj=new child();
	obj.printData();
	obj.Disp();
	print(obj.str1);
}
